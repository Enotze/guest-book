<?php
/**
 * Created by PhpStorm.
 * User: �������������
 * Date: 28.10.2015
 * Time: 17:55
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class UserProfile extends ActiveRecord
{
    public static function getDb(){
        return \Yii::$app->guests;
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
    public function rules()
    {
        $rules = [
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique',
                'targetClass' => UserProfile::className(),
                'message' => 'This email is already occupied.'],
            ['gender', 'boolean'],
            ['ban', 'boolean'],
            ['birthday','string'],
            [['firstName', 'secondName', 'middleName'], 'string', 'max' => 32],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];

        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'firstName' => 'First name',
            'secondName' => 'Second name',
            'middleName' => 'Middle name',
            'birthday' => 'Birthday',
            'gender' => 'Gender',
            'imageFile' => 'Avatar',
            'ban' => 'Ban',
        ];
    }

}