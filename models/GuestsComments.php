<?php

namespace app\models;

use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Connection;
use Yii;


class GuestsComments extends ActiveRecord
{
	public $verifyCode;

    public function rules()
    {
        $rules = [
            [['name', 'email', 'comment'], 'required'],
            ['email', 'email'],
            ['anonymity', 'boolean'],
            ['parent_id', 'integer'],
            ['avatar','string'],
        ];

        if(Yii::$app->user->isGuest)
        {
            $rules[] = ['verifyCode' , 'captcha'];
        }

        return $rules;
    }

	public static function tableName()
    {
        return 'guest_book';
    }
	
	public static function getDb(){
       return \Yii::$app->guests;
    }

}
