<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guest_book".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property string $dateofcomment
 * @property string $dateofeditcomment
 * @property integer $anonymity
 * @property string $avatar
 */
class GuestBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guest_book';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('guests');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'user_id', 'name', 'email', 'comment', 'dateofcomment', 'dateofeditcomment', 'anonymity'], 'required'],
            [['parent_id', 'user_id', 'anonymity'], 'integer'],
            [['name', 'email', 'comment', 'avatar'], 'string'],
            [['dateofcomment', 'dateofeditcomment'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'comment' => 'Comment',
            'dateofcomment' => 'Date of comment',
            'dateofeditcomment' => 'Date of edit comment',
            'anonymity' => 'Anonymity',
            'avatar' => 'Avatar',
        ];
    }
}
