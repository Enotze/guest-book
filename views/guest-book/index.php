<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GuestBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Guest Book';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guest-book-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            ['class' => 'yii\grid\ActionColumn'],

            'id',
            'parent_id',
            'user_id',
            [
                'label' => 'Profile',
                'format' => 'raw',
                'value' => function($data){
                    return Html::a(
                        'Move',
                        ['site/profile', 'id' => $data->user_id],
                        [
                            'target' => '_blank'
                        ]
                    );
                }
            ],
            'name:ntext',
            'email:ntext',
            'comment:ntext',
            'dateofcomment',
            'dateofeditcomment',
            'anonymity',
            'avatar:image',


        ],
    ]); ?>

</div>
