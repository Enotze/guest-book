<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\GuestBook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guest-book-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'email')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'dateofcomment')->widget(DateTimePicker::className(),[
                                                        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                        'pluginOptions' => [
                                                            'autoclose'=>true,
                                                            'format' => 'yyyy-M-dd hh:ii'
                                                        ]
    ]);?>

    <?= $form->field($model, 'dateofeditcomment')->widget(DateTimePicker::className(),[
                                                            'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                            'pluginOptions' => [
                                                                'autoclose'=>true,
                                                                'format' => 'yyyy-M-dd hh:ii'
                                                            ]
                                                        ]);?>
    <?= $form->field($model, 'anonymity')->textInput() ?>

    <?= $form->field($model, 'avatar')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
