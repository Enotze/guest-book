<?php
/**
 * Created by PhpStorm.
 * User: �������������
 * Date: 28.10.2015
 * Time: 18:50
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;


$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->isAdmin && $model->ban): ?>
<h1>Profile banned</h1>
<?php endif; ?>

<div class="row">
    <div class="col-lg-5">
        <h3>Username: <?= Html::encode($model->username) ?></h3>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?php if(!Yii::$app->user->isGuest)
                if(\Yii::$app->user->identity->isAdmin)
                    echo $form->field($model, 'ban')->radioList([0 => 'Not banned', 1 => 'Banned'], ['inline'=>true]); ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'secondName') ?>
        <?= $form->field($model, 'firstName') ?>
        <?= $form->field($model, 'middleName') ?>
        <?= $form->field($model, 'birthday')->widget(DatePicker::className(),[
                                                        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                                                        'language'=>'ru',
                                                        'pluginOptions' => [
                                                            'autoclose'=>true,
                                                            'format' => 'yyyy-mm-dd'
                                                        ]
                                                    ]); ?>
        <?php $list = [0 => 'Female', 1 => 'Male']; ?>
        <?= $form->field($model, 'gender')->radioList($list, ['inline'=>true]); ?>
        <p><strong>Avatar</strong></p>
        <?php if($model->imageFile): ?>
        <?= Html::img("../../images/$model->imageFile", ['alt'=>'avatar','width'=>"300px", 'height'=>"300px",
             'style'=>"border-radius:8px; object-fit:cover; margin-bottom:15px;"]) ?>
        <br/>
        <?= Html::a('Delete Avatar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'style' => 'width:300px;margin-bottom:15px',
            'data' => [
                'confirm' => 'Are you sure you want to delete this avatar?',
            ],
        ]) ?>
        <?php endif;?>
        <?= $form->field($model, 'imageFile')->widget(FileInput::className(),[
            'options' => ['accept' => 'image/*','maxFileCount' => 1],
        ])->label(false); ?>
        <div class="form-group">
            <?= Html::submitButton('Update profile', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>