<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
/* @var $this yii\web\View */
/* @var $model app\models\RegForm */
/* @var $form ActiveForm */

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reg">
	<h1><?= Html::encode($this->title) ?></h1>
	
    <?php $form = ActiveForm::begin([
        'id' => 'reg-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username') ?>
		
        <?= $form->field($model, 'email') ?>
		
        <?= $form->field($model, 'password')->passwordInput() ?>
		
		<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                       // 'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

        <div class="form-group">
			<div class="col-lg-offset-1 col-lg-11">
				<?= Html::submitButton('Registration', ['class' => 'btn btn-primary']) ?>
			</div>
		</div>
    <?php ActiveForm::end(); ?>
	
</div><!-- site-reg -->