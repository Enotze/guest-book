<?php
/**
 * Created by PhpStorm.
 * User: �������������
 * Date: 18.10.2015
 * Time: 18:21
 */

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guest Book', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="comment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            'comment',
            'dateofcomment',
            'dateofeditcomment',
            'anonymity',
        ],
    ]) ?>

</div>
