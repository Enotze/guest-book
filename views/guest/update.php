<?php
/**
 * Created by PhpStorm.
 * User: �������������
 * Date: 18.10.2015
 * Time: 15:16
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;




$this->title = 'Update comment: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Guest Book', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_form', [
    'model' => $model,
]) ?>