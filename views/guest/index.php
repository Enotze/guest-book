<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\LinkPager;
use yii\captcha\Captcha;
use app\models\UserProfile;

$this->registerJsFile('web/js/toggle.js');

$this->title = 'Guest Book';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<h3>Last comments</h3>

<?php
$isAdmin = 0;
if(!Yii::$app->user->isGuest)
	if(\Yii::$app->user->identity->isAdmin)
		$isAdmin = 1;
//$photo = (Yii::$app->user->identity->profile);
//print_r($photo);
//echo $photo['photo'];
?>

<?php foreach ($guestscomments as $guestcomment): ?>
	<div style="margin:0 0 30px 100px">
		<?php
			$user = UserProfile::findOne($guestcomment->user_id);
			$avatar = '../../images/';
			isset($user->imageFile) ? $avatar = $avatar.$user->imageFile : $avatar = $avatar.'default.jpg';
			if(($guestcomment->avatar)!= NULL) $avatar = $guestcomment->avatar;
		?>
		<div style="float:left;margin-left:-100px">
			<?= Html::img("$avatar", ['alt'=>'avatar','width'=>"75px", 'height'=>"75px",
				'style'=>"border-radius:8px; object-fit:cover;"]) ?>
			<?php if(isset($user->isAdmin) && ($user->isAdmin)) { ?>
				<br>
				<p style="text-align: center"><strong>Admin</strong></p>
			<?php } ?>
		</div>
		<p style="font-size:large;margin-bottom:0;">
				<strong>
					<?= Html::encode("{$guestcomment->name}") ?>
				</strong>
		</p>
		<p style="color:grey;margin-bottom:0;"><?= Html::encode("{$guestcomment->dateofcomment}")?></p>
		<p style="margin-bottom:0;"><?= Html::encode("{$guestcomment->comment}") ?></p>

		<?php if(!Yii::$app->user->isGuest && !Yii::$app->user->identity->ban): ?>
		<?= Html::a('Reply',['guest/index','#' => 'form','parent_id' => $guestcomment->id], ['class' => 'btn btn-primary']) ?>
		<?php endif; ?>

		<?php if($isAdmin): ?>
			<?= Html::a('Delete', ['delete', 'id' => $guestcomment->id], [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => 'Are you sure you want to delete this comment?',
						],
					]) ?>
			<?= Html::a('View', ['view', 'id' => $guestcomment->id], [
				'class' => 'btn btn-primary',
			]) ?>
		<?php endif; ?>
	</div>

	<?php foreach ($replys as $reply){ ?>
		<?php if($reply->parent_id == $guestcomment->id){ ?>
			<div style="margin:0 0 15px 175px">
				<?php
				$user = UserProfile::findOne($reply->user_id);
				$avatar = '../../images/';
				isset($user->imageFile) ? $avatar = $avatar.$user->imageFile : $avatar = $avatar.'default.jpg';
				if(($reply->avatar)!= NULL) $avatar = $reply->avatar;
				?>
				<div style="float:left;margin-left:-75px">
					<?= Html::img("$avatar", ['alt'=>'avatar','width'=>"50px", 'height'=>"50px",
					'style'=>"border-radius:8px; object-fit:cover;"]) ?>
					<?php if(isset($user->isAdmin) && ($user->isAdmin)) { ?>
						<br>
						<p style="text-align: center"><strong>Admin</strong></p>
					<?php } ?>
				</div>
				<p style="font-size:large;margin-bottom:0;">
					<strong>
						<?= Html::encode("{$reply->name}") ?>
					</strong>
				</p>
				<p style="color:grey;margin-bottom:0;"><?= Html::encode("{$reply->dateofcomment}")?></p>
				<p style="margin-bottom:0;"><?= Html::encode("{$reply->comment}") ?></p>

				<?php if($isAdmin){ ?>
						<?= Html::a('Delete', ['delete', 'id' => $reply->id], [
									'class' => 'btn btn-danger',
									'data' => [
										'confirm' => 'Are you sure you want to delete this comment?',
									],
								]) ?>
								<?= Html::a('View', ['view', 'id' => $reply->id], [
									'class' => 'btn btn-primary',
								]) ?>
				<?php }; ?>
			</div>
		<?php }; ?>
	<?php }; ?>
	<hr>
<?php endforeach; ?>


<?= LinkPager::widget(['pagination' => $pagination]) ?>

<?php if(!isset(Yii::$app->user->identity->ban) || !Yii::$app->user->identity->ban): ?>
<a name="form"></a>
<div class="row" >
    <div class="col-lg-5">
		<h3>Leave comment</h3>
		<?php $form = ActiveForm::begin(); ?>
		<?= $form->field($model, 'parent_id')->hiddenInput()->label(false, ['style'=>'display:none']);?>
		<?= $form->field($model, 'name') ?>
		<?= $form->field($model, 'email') ?>
		<?= $form->field($model, 'comment')->textArea(['rows' => 6]) ?>
		<?php if(Yii::$app->user->isGuest): ?>
		<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>
		<?php endif; ?>
		<?= $form->field($model, 'anonymity')->checkbox(); ?>
		<div class="form-group">
			<?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
<?php endif; ?>