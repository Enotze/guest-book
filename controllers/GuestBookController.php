<?php

namespace app\controllers;

use Yii;
use app\models\GuestBook;
use app\models\GuestBookSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GuestBookController implements the CRUD actions for GuestBook model.
 */
class GuestBookController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all GuestBook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuestBookSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin) {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else {
            return $this->redirect(['guest/index']);
        }
    }

    /**
     * Displays a single GuestBook model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin) {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }else {
            return $this->redirect(['guest/index']);
        }
    }


    /**
     * Updates an existing GuestBook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->dateofeditcomment=='0000-00-00 00:00:00') $model->dateofeditcomment = date('Y-m-d H:i:s', time());
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin) {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }else {
                return $this->redirect(['guest/index']);
            }
        }
    }

    /**
     * Deletes an existing GuestBook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GuestBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuestBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GuestBook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
