<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\GuestsComments;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;


class GuestController extends Controller
{
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->dateofeditcomment = date('Y-m-d H:i:s', time());
			$model->save(false);
			return $this->redirect(['view', 'id' => $model->id]);
		}else {
			//print_r($model);
			//print_r($model->errors);
			if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin) {
				return $this->render('update', [
					'model' => $model,
				]);
			}else {
				return $this->redirect(['index']);
			}
		}
	}

	public function actionView($id)
	{
		if(!Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin) {
			return $this->render('view', [
				'model' => $this->findModel($id),
			]);
		}else {
			return $this->redirect(['index']);
		}
	}

	protected function findModel($id)
	{
		if (($model = GuestsComments::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

    public function actionIndex($parent_id = 0)
    {
        $model = new GuestsComments;
		if(isset($parent_id))$model->parent_id = $parent_id;

        if ($model->load(Yii::$app->request->post()) && $model->validate())
		{
			$model->dateofcomment = date('Y-m-d H:i:s',time());
			Yii::$app->user->id ? $model->user_id = Yii::$app->user->id : $model->user_id = 0;
			if (isset(Yii::$app->user->identity->profile)) {
				$profile = Yii::$app->user->identity->profile;
				if(isset($profile['photo'])) $model->avatar = $profile['photo'];
			}
			$model->save(false);
			return $this->redirect(['guest/index']);
        }

		$query = $model->find();

		$pagination = new Pagination([
			//'defaultPageSize' => 5,
			'defaultPageSize' => (Yii::$app->params['pageSize']),
			'totalCount' => $query->where('parent_id = 0')->count(),
		]);
				
		$guestscomments = $query
			->offset($pagination->offset)
			->limit($pagination->limit)
			->where('parent_id = 0')
			->orderby('id desc')
			->all();

		$replys = $model->find()
			->where('parent_id <> 0')
			->orderby('id asc')
			->all();
		
		foreach ($guestscomments as $guestcomment)
		{
			if ($guestcomment->anonymity==1) $guestcomment->name='Anonym';
		}
		
		if(!Yii::$app->user->isGuest)
		{
			$model->name = Yii::$app->user->identity->username;
			$model->email = Yii::$app->user->identity->email;
		}
		
		return $this->render('index', [
			'guestscomments' => $guestscomments,
			'pagination' => $pagination,
			'model' => $model,
			'replys' => $replys,
		]);
        
    }
}